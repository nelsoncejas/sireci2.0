                  <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-block"><br>
                                    <div class="table-responsive">
                                        <table class="table center-aligned-table">
                                            <thead>
                                                <tr class="text-primary">
                                                    <th>Nro</th>
                                                    <th>Descripcion</th>
                                                    <th>Cliente</th>
                                                    <th>Ubicacion</th>
                                                    <th>Fecha Alta</th>
                                                    <th>Estado</th>
                                                    <th>Precio</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="">
                                                    <td>034</td>
                                                    <td>Designs</td>
                                                    <td>Client</td>
                                                    <td>53275531</td>
                                                    <td>12 May 2017</td>
                                                    <td><label class="badge badge-success">Approved</label></td>
                                                    <td>$349</td>
                                                    <td><a href="#" class="btn btn-primary btn-sm">Modificar</a></td>
                                                    <td><a href="#" class="btn btn-danger btn-sm">Eliminar</a></td> 
                                                </tr>
                                                <tr class="">
                                                    <td>035</td>
                                                    <td>Designs</td>
                                                    <td>Client</td>
                                                    <td>53275531</td>
                                                    <td>12 May 2017</td>
                                                    <td><label class="badge badge-warning">Approved</label></td>
                                                    <td>$349</td>
                                                    <td><a href="#" class="btn btn-primary btn-sm">Modificar</a></td>
                                                    <td><a href="#" class="btn btn-danger btn-sm">Eliminar</a></td> 
                                                </tr>
                                                <tr class="">
                                                    <td>036</td>
                                                    <td>Designs</td>
                                                    <td>Client</td>
                                                    <td>53275531</td>
                                                    <td>12 May 2017</td>
                                                    <td><label class="badge badge-success">Approved</label></td>
                                                    <td>$349</td>
                                                    <td><a href="#" class="btn btn-primary btn-sm">Modificar</a></td>
                                                    <td><a href="#" class="btn btn-danger btn-sm">Eliminar</a></td> 
                                                </tr>
                                                <tr class="">
                                                    <td>037</td>
                                                    <td>Designs</td>
                                                    <td>Client</td>
                                                    <td>53275531</td>
                                                    <td>12 May 2017</td>
                                                    <td><label class="badge badge-danger">Rejected</label></td>
                                                    <td>$349</td>
                                                    <td><a href="#" class="btn btn-primary btn-sm">Modificar</a></td>
                                                    <td><a href="#" class="btn btn-danger btn-sm">Eliminar</a></td> 
                                                </tr>
                                                <tr class="">
                                                    <td>038</td>
                                                    <td>Designs</td>
                                                    <td>Client</td>
                                                    <td>53275531</td>
                                                    <td>12 May 2017</td>
                                                    <td><label class="badge badge-success">Approved</label></td>
                                                    <td>$349</td>
                                                    <td><a href="#" class="btn btn-primary btn-sm">Modificar</a></td>
                                                    <td><a href="#" class="btn btn-danger btn-sm">Eliminar</a></td> 
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
<?php include"sis_header.php"; ?>
<body>
    <div class=" container-scroller">
        <!--Navbar-->
        <?php include"navbar.php"; ?>
        <!--End navbar-->
        <div class="container-fluid">

            <div class="row row-offcanvas row-offcanvas-right">

                <!-- SIDEBAR -->

                <?php include"sis_sidebar.php"; ?>

                <!-- SIDEBAR ENDS -->



            <div class="content-wrapper">

                    <h3 class="text-primary mb-4">INICIO</h3>

                    <div class="row mb-2">

                        <div class="col-lg-12">

                            <div class="card">

                                <div class="card-block"><br>

                                    <h5 class="card-title mb-4">Contenedores O ALGO QUE APAREZCA EN EL HOME</h5>

                                    <div class="table-responsive">

                                        <table class="table center-aligned-table">

                                            <thead>

                                                <tr class="text-primary">

                                                    <th>Nro</th>

                                                    <th>Descripcion</th>

                                                    <th>Cliente</th>

                                                    <th>Ubicacion</th>

                                                    <th>Fecha Alta</th>

                                                    <th>Estado</th>

                                                    <th>Precio</th>

                                                    <th></th>

                                                    <th></th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <tr class="">

                                                    <td>034</td>

                                                    <td>Designs</td>

                                                    <td>Client</td>

                                                    <td>53275531</td>

                                                    <td>12 May 2017</td>

                                                    <td><label class="badge badge-success">Approved</label></td>

                                                    <td>$349</td>

                                                    <td><a href="#" class="btn btn-primary btn-sm">Manage</a></td>

                                                    <td><a href="#" class="btn btn-danger btn-sm">Remove</a></td> 

                                                </tr>

                                                <tr class="">

                                                    <td>035</td>

                                                    <td>Designs</td>

                                                    <td>Client</td>

                                                    <td>53275531</td>

                                                    <td>12 May 2017</td>

                                                    <td><label class="badge badge-warning">Approved</label></td>

                                                    <td>$349</td>

                                                    <td><a href="#" class="btn btn-primary btn-sm">Manage</a></td>

                                                    <td><a href="#" class="btn btn-danger btn-sm">Remove</a></td> 

                                                </tr>

                                                <tr class="">

                                                    <td>036</td>

                                                    <td>Designs</td>

                                                    <td>Client</td>

                                                    <td>53275531</td>

                                                    <td>12 May 2017</td>

                                                    <td><label class="badge badge-success">Approved</label></td>

                                                    <td>$349</td>

                                                    <td><a href="#" class="btn btn-primary btn-sm">Manage</a></td>

                                                    <td><a href="#" class="btn btn-danger btn-sm">Remove</a></td> 

                                                </tr>

                                                <tr class="">

                                                    <td>037</td>

                                                    <td>Designs</td>

                                                    <td>Client</td>

                                                    <td>53275531</td>

                                                    <td>12 May 2017</td>

                                                    <td><label class="badge badge-danger">Rejected</label></td>

                                                    <td>$349</td>

                                                    <td><a href="#" class="btn btn-primary btn-sm">Manage</a></td>

                                                    <td><a href="#" class="btn btn-danger btn-sm">Remove</a></td> 

                                                </tr>

                                                <tr class="">

                                                    <td>038</td>

                                                    <td>Designs</td>

                                                    <td>Client</td>

                                                    <td>53275531</td>

                                                    <td>12 May 2017</td>

                                                    <td><label class="badge badge-success">Approved</label></td>

                                                    <td>$349</td>

                                                    <td><a href="#" class="btn btn-primary btn-sm">Manage</a></td>

                                                    <td><a href="#" class="btn btn-danger btn-sm">Remove</a></td> 

                                                </tr>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>



                <footer class="footer">

                    <div class="container-fluid clearfix">

                        <span class="float-right">

                          <a href="#">SIRECI</a> &copy; 2017

                      </span>

                    </div>

                </footer>

            </div>

        </div>



    </div>

</body>



</html>
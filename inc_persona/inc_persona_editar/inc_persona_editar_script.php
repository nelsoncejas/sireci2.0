<script type="text/javascript"> 
$('#btn_editar_persona').on('click',function(){
  editar_persona();
});   

function editar_persona(){
  var dni = $('#dni').val();
  var apellido = $('#apellido').val();
  var nombre = $('#nombre').val();
    $.ajax({
     type: "POST",
        url: "persona_alta_recibe.php",
        data: {dni:dni, apellido: apellido, nombre:nombre},
        dataType: "html",
        beforeSend: function(){
          $("#resultado_persona_listado").empty();
          $("#resultado_persona_listado").html('<div align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
        },
        error: function(){
          $("#resultado_persona_listado").empty();
          $("#resultado_persona_listado").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error, No se pueden cargar los datos</strong></div>');              
        },
        success: function(data){                                                    
         listar_personas();                                                     
        }
      });
    } 
</script>
<div class="modal fade" id="modal_editar_persona">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Crear Persona</h4>
			</div>
			<div class="modal-body">
				<?php include "inc_persona/inc_persona_alta/inc_persona_alta_grid.php"; ?>				
			</div>
			<div class="modal-footer">
				<button type="button"  class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button data-dismiss="modal" type="button"  id="btn_crear_persona" class="btn btn-primary">Guardar</button>
			</div>
		</div>
	</div>
</div>
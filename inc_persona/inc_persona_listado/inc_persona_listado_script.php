<script type="text/javascript">
    $(document).ready(function() {
     listar_personas();
    });

    function listar_personas(){
      $.ajax({
        type: "POST",
        url: "persona_listado_recibe.php",
        data: {},
        dataType: "html",
        beforeSend: function(){
          $("#resultado_persona_listado").empty();
          $("#resultado_persona_listado").html('<div align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
        },
        error: function(){
          $("#resultado_persona_listado").empty();
          $("#resultado_persona_listado").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error, No se pueden cargar los datos</strong></div>');              
        },
        success: function(data){                                                    
          $("#resultado_persona_listado").empty();
          $("#resultado_persona_listado").append(data);                                                     
        }
      });
    } 
</script>
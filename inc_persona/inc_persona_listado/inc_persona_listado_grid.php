<div class="row mb-2">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block"><br>
                <div class="table-responsive">
                    <table class="table center-aligned-table">
                        <thead>
                            <tr class="text-primary">
                                <th>id</th>
                                <th>dni</th>
                                <th>apellido</th>
                                <th>nombre</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php while ($row_persona_select= mysqli_fetch_array($q_persona_select)) {?>
                            <tr>
                                <td><?php echo $row_persona_select['idpersona'] ?></td>
                                <td><?php echo $row_persona_select['dni'] ?></td>    
                                <td><?php echo $row_persona_select['apellido'] ?></td>    
                                <td><?php echo $row_persona_select['nombre'] ?></td>    
                                <td><a data-toggle="modal" href='#modal_editar_persona' class="btn btn-primary btn-sm">Modificar</a></td>
                                <td><a href="#" class="btn btn-danger btn-sm">Eliminar</a></td> 
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
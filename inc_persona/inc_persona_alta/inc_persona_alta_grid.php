<?php
  if (!isset($dni)) {$dni='';}
  if (!isset($apellido)) {$apellido='';} 
  if (!isset($nombre)) {$nombre='';} 
?>

<form>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon"><i class="fa fa-user"></i></span>
      <input type="text" class="form-control p_input" placeholder="DNI" name="dni" id="dni" value="<?php echo $dni ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span>
      <input type="text" class="form-control p_input" placeholder="Apellido" name="apellido" id="apellido" value="<?php echo $apellido ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span>
      <input type="text" class="form-control p_input" placeholder="Nombre" name="nombre" id="nombre" value="<?php echo $nombre ?>">
    </div>
  </div>
</form>

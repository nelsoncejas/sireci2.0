
<?php include"header.php"; ?>
<body>
  <div class="container-scroller">
    <div class="container-fluid">
      <div class="row">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center">
          <div class="card col-lg-4 offset-lg-4">
            <div class="card-block">
              <h3 class="card-title text-primary text-left mb-5 mt-4">Registrarse</h3>
              <form>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control p_input" placeholder="Empresa" id="empresa">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span>
                    <input type="text" class="form-control p_input" placeholder="Cuit" id="cuit">
                  </div>
                </div>
                 <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span>
                    <input type="text" class="form-control p_input" placeholder="Direccion" id="direccion">
                  </div>
                </div>
                  <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span>
                    <input type="text" class="form-control p_input" placeholder="Usuario" id="user" id="user">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="text" class="form-control p_input" placeholder="Password" id="pass">
                  </div>
                </div>
                <button type="submit" id="registrar" class="btn btn-primary">Registrar</button>
                <button type="submit" id="enviar_postman" class="btn btn-primary">enviar_postman</button>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
<?php include"registrar_script.php"; ?>

<?php include"sis_header.php"; ?>
<body>
    <div class=" container-scroller">
        <!--Navbar-->
        <?php include"navbar.php"; ?>
        <!--End navbar-->
        <div class="container-fluid">
            <div class="row row-offcanvas row-offcanvas-right">
                <!-- SIDEBAR -->
                <?php include"sis_sidebar.php"; ?>
					<!--Inicio tabla-->
		        <div class="content-wrapper">
					<div class="header">     
      					<h3 class="text-primary mb-4">Gestión de Recicladores</h3>
    				</div>
    				<nav class="navbar navbar-toggleable-md navbar-toggleable-md bg-faded">
						  <a class="navbar-brand" data-toggle="tab" href="#">Listados</a>
						  <a class="navbar-brand" data-toggle="tab" href="#">Modificar</a>
					  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
					    	<form class="form-inline my-2 my-lg-0">
					    		  <input class="form-control mr-sm-2" type="text" placeholder="Buscar">
					      		<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
					   		 </form>
					  	</div>
					  	<a data-toggle="modal" href='#modal_crear_persona'>
					    <button id="" class="btn btn-outline-success my-2 my-sm-0" type="">
					  		Nuevo
					    </button>
					    </a>
					</nav>
					<div id="resultado_persona_listado"></div>
					<?php// include"contenedores_grid.php"; ?>
				</div>
			</div>	
  		</div>
    </div>
<?php include "inc_persona/inc_persona_listado/inc_persona_listado_script.php"; ?>
<!--ALTA PERSONA-->
<?php include "inc_persona/inc_persona_alta/inc_persona_alta_modal.php"; ?>
<?php include "inc_persona/inc_persona_alta/inc_persona_alta_script.php"; ?>
<!-- FIN ALTA PERSONA-->
<!--EDITAR PERSONA-->
<?php//include "inc_persona/inc_persona_editar/inc_persona_editar_modal.php"; ?>
<?php //include "inc_persona/inc_persona_editar/inc_persona_editar_script.php"; ?>
<!-- FIN EDITAR PERSONA-->


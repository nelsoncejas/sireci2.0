<?php include"header.php"; ?>
<body>
  <div class="container-scroller">
    <div class="container-fluid">
      <div class="row">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center">
          <div class="card col-lg-4 offset-lg-4">
            <div class="card-block">
              <h3 class="card-title text-primary text-left mb-5 mt-4">Login</h3>
              <form  ACTION="principal.php" METHOD="POST" id="conecta">
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control p_input" placeholder="Username">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" class="form-control p_input" placeholder="Password">
                  </div>
                </div>
                <div class="text-center">
                  <button href="" type="submit" id="ingresar" class="btn btn-primary">Login</button>
                </div>
                <br>
                
                <p>
                 <a href="inc_usuario_alta.php">Olvidaste tu contraseña?</a>
                 </p>
              
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
<?php include"login_script.php"; ?>